<TeXmacs|2.1.1>

<style|<tuple|beamer|chinese|doc>>

<\body>
  <screens|<\shown>
    <tit|Binary Operators>

    <\big-table*|<tabular|<tformat|<table|<row|<cell|<math|\<amalg\>>>|<cell|<markup|\\amalg>>|<cell|<math|\<cup\>>>|<cell|<markup|\\cup>>|<cell|<math|\<oplus\>>>|<cell|<markup|\\oplus>>|<cell|<math|\<times\>>>|<cell|<markup|\\times>>>|<row|<cell|>|<cell|<key|%><key|var><key|var><key|var>>|<cell|>|<cell|>|<cell|>|<cell|<key|@><key|+>>|<cell|>|<cell|>>|<row|<cell|<math|\<ast\>>>|<cell|<markup|\\ast>>|<cell|<math|\<dagger\>>>|<cell|<markup|\\dagger>>|<cell|<math|\<oslash\>>>|<cell|<markup|\\oslash>>|<cell|<math|\<triangleleft\>>>|<cell|<markup|\\triangleleft>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|\<bigcirc\>>>|<cell|<markup|\\bigcirc>>|<cell|<math|\<ddagger\>>>|<cell|<markup|\\ddagger>>|<cell|<math|\<otimes\>>>|<cell|<markup|\\otimes>>|<cell|<math|\<triangleright\>>>|<cell|<markup|\\triangleright>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|\<bigtriangledown\>>>|<cell|<markup|\\bigtriangledown>>|<cell|<math|\<diamond\>>>|<cell|<markup|\\diamond>>|<cell|<math|\<pm\>>>|<cell|<markup|\\pm>>|<cell|<math|\<trianglelefteq\>>>|<cell|<todo|\\unlhd*>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|\<bigtriangleup\>>>|<cell|<markup|\\bigtriangleup>>|<cell|<math|\<div\>>>|<cell|<markup|\\div>>|<cell|<math|\<vartriangleright\>>>|<cell|<todo|\\rhd*>>|<cell|<math|\<trianglerighteq\>>>|<cell|<todo|\\unrhd*>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|\<bullet\>>>|<cell|<markup|\\bullet>>|<cell|<math|\<vartriangleleft\>>>|<cell|<todo|\\lhd*>>|<cell|<math|\<setminus\>>>|<cell|<markup|\\setminus>>|<cell|<math|\<uplus\>>>|<cell|<markup|\\uplus>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|\<cap\>>>|<cell|<markup|\\cap>>|<cell|<math|\<mp\>>>|<cell|<markup|\\mp>>|<cell|<math|\<sqcap\>>>|<cell|<markup|\\sqcap>>|<cell|<math|\<vee\>>>|<cell|<markup|\\vee>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|\<cdot\>>>|<cell|<markup|\\cdot>>|<cell|<math|\<odot\>>>|<cell|<markup|\\odot>>|<cell|<math|\<sqcup\>>>|<cell|<markup|\\sqcup>>|<cell|<math|\<wedge\>>>|<cell|<markup|\\wedge>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|\<circ\>>>|<cell|<markup|\\circ>>|<cell|<math|\<ominus\>>>|<cell|<markup|\\ominus>>|<cell|<math|\<star\>>>|<cell|<markup|\\star>>|<cell|<math|\<wr\>>>|<cell|<markup|\\wr>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>>>>>
      \<#4E8C\>\<#5143\>\<#64CD\>\<#4F5C\>\<#7B26\>
    </big-table*>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|table>
      <\tuple|normal>
        \<#4E8C\>\<#5143\>\<#64CD\>\<#4F5C\>\<#7B26\>
      </tuple|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>