<TeXmacs|2.1.1>

<style|<tuple|generic|chinese>>

<\body>
  <\hide-preamble>
    <assign|myspace|http://git.tmml.wiki/XmacsLabs/planet/raw/main/\<#58A8\>\<#8005\>\<#5B9E\>\<#9A8C\>\<#5BA4\>/>

    <assign|gitlink|<macro|name|<hlink|<arg|name>|<merge|<value|myspace>|<arg|name>|.tm>>>>
  </hide-preamble>

  <doc-data|<doc-title|\<#58A8\>\<#8005\>\<#5B9E\>\<#9A8C\>\<#5BA4\>>>

  <section*|\<#6587\>\<#6863\>\<#5217\>\<#8868\>>

  <\itemize>
    <item><gitlink|\<#8D22\>\<#52A1\>\<#516C\>\<#5F00\>>
  </itemize>
</body>

<\initial>
  <\collection>
    <associate|page-medium|papyrus>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#6587\>\<#6863\>\<#5217\>\<#8868\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>