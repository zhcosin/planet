<TeXmacs|2.1.1>

<style|<tuple|article|chinese>>

<\body>
  <doc-data|<\doc-title>
    <chapter*|<label|activity-name>\<#90D1\>\<#5DDE\>\<#5927\>\<#5B66\>2022\<#5E74\>\<#6570\>\<#5B66\>\<#5206\>\<#6790\>\<#8003\>\<#7814\>\<#8BD5\>\<#9898\>\<#53CA\>\<#89E3\>\<#7B54\>>

    by zhcosin
  </doc-title>>

  <\enumerate-numeric>
    <item>\<#8BA1\>\<#7B97\> <math|lim<rsub|x\<rightarrow\>0><frac|<big|int><rsub|0><rsup|x>e<rsup|t<rsup|2>>d
    t-x|ln<around*|(|1+x<rsup|3>|)>>>.

    <\solution*>
      \<#6613\>\<#77E5\>\<#5206\>\<#5B50\>\<#5206\>\<#6BCD\>\<#5728\>
      <math|x\<rightarrow\>0> \<#65F6\>\<#5747\>\<#4E3A\>\<#65E0\>\<#7A77\>\<#5C0F\>\<#FF0C\>\<#7531\>\<#6D1B\>\<#5FC5\>\<#8FBE\>\<#6CD5\>\<#5219\>\<#6709\>

      <\eqnarray*>
        <tformat|<table|<row|<cell|lim<rsub|x\<rightarrow\>0><frac|<big|int><rsub|0><rsup|x>e<rsup|t<rsup|2>>d
        t-x|ln<around*|(|1+x<rsup|3>|)>>>|<cell|=>|<cell|lim<rsub|x\<rightarrow\>0><frac|<around*|(|e<rsup|x<rsup|2>>-1|)><around*|(|1+x<rsup|3>|)>|3x<rsup|2>>>>|<row|<cell|>|<cell|=>|<cell|lim<rsub|x\<rightarrow\>0><frac|1+x<rsup|3>|3>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|3>>>>>
      </eqnarray*>
    </solution*>

    <item>\<#8BA1\>\<#7B97\> <math|lim<rsub|n\<rightarrow\>\<infty\>><around*|(|cos<frac|x|2>cos<frac|x|2<rsup|2>>\<cdots\>cos<frac|x|2<rsup|n>>|)>>\<#FF0C\>\<#5176\>\<#4E2D\>
    <math|x\<in\>R>.

    <\solution*>
      \<#5F0F\>\<#5B50\>\<#4E58\>\<#4EE5\> <math|sin<frac|x|2<rsup|n>>>
      \<#540E\>\<#7531\>\<#6B63\>\<#5F26\>\<#7684\>\<#500D\>\<#89D2\>\<#516C\>\<#5F0F\>\<#4FBF\>\<#4F1A\>\<#53D1\>\<#751F\>\<#8FDE\>\<#9501\>\<#53D1\>\<#751F\>\<#FF0C\>\<#6700\>\<#540E\>\<#7ED3\>\<#679C\>\<#4E3A\>
      <math|<frac|1|2<rsup|n>>sin x>\<#FF0C\>\<#6240\>\<#4EE5\>\<#6709\>

      <\eqnarray*>
        <tformat|<table|<row|<cell|lim<rsub|n\<rightarrow\>\<infty\>><around*|(|cos<frac|x|2>cos<frac|x|2<rsup|2>>\<cdots\>cos<frac|x|2<rsup|n>>|)>>|<cell|=>|<cell|lim<rsub|n\<rightarrow\>\<infty\>><frac|sin
        x|2<rsup|n>sin<frac|x|2<rsup|n>>>>>|<row|<cell|>|<cell|=>|<cell|lim<rsub|n\<rightarrow\>\<infty\>><frac|sin
        x|x\<cdot\><frac|sin<frac|x|2<rsup|n>>|<frac|x|2<rsup|n>>>>>>|<row|<cell|>|<cell|=>|<cell|<frac|sin
        x|x>>>>>
      </eqnarray*>
    </solution*>

    <item>\<#8BA1\>\<#7B97\> <math|<big|int><rsub|0><rsup|<frac|\<pi\>|2>><frac|sin
    x|sin x+cos x>d x>.

    <\solution*>
      \<#8BB0\> <math|A=<big|int><rsub|0><rsup|<frac|\<pi\>|2>><frac|sin
      x|sin x+cos x>d x>, <math|B=<big|int><rsub|0><rsup|<frac|\<pi\>|2>><frac|cos
      x|sin x+cos x>d x>\<#FF0C\>\<#5219\> <math|A+B=<frac|\<pi\>|2>>.
      \<#800C\>\<#5728\>\<#53D8\>\<#6362\> <math|t=<frac|\<pi\>|2>-x>
      \<#4E0B\>\<#FF0C\>\<#6709\>\ 

      <\equation*>
        A=<big|int><rsub|0><rsup|<frac|\<pi\>|2>><frac|sin x|sin x+cos x>d
        x=-<big|int><rsub|<frac|\<pi\>|2>><rsup|0><frac|cos t|cos t+sin x>d
        t=B
      </equation*>

      \<#6240\>\<#4EE5\> <math|A=B=<frac|\<pi\>|4>>.
    </solution*>

    <item>\<#8BBE\>\<#533A\>\<#57DF\> <math|D=<around*|{|<around*|(|x,y|)>\<in\>R<rsup|2>:x<rsup|2>+y<rsup|2>\<leqslant\>1|}>>,
    \<#8BA1\>\<#7B97\>

    <\equation*>
      <big|iint><rsub|D><around*|\||x-y|\|>d x d y
    </equation*>

    <item>\<#5224\>\<#65AD\>\<#7EA7\>\<#6570\>
    <math|<big|sum><rsub|n=2><rsup|\<infty\>><frac|1|ln n<rsup|ln n>>>
    \<#7684\>\<#655B\>\<#6563\>\<#6027\>.

    <item>\<#8BA1\>\<#7B97\> <math|<big|int><rsub|0><rsup|1><frac|x-1|ln x>d
    x>.

    <\solution*>
      \<#4F5C\>\<#53D8\>\<#6362\> <math|t=ln x>, \<#6709\>

      <\eqnarray*>
        <tformat|<table|<row|<cell|<big|int><rsub|0><rsup|1><frac|x-1|ln x>d
        x>|<cell|=>|<cell|<big|int><rsub|-\<infty\>><rsup|0><frac|e<rsup|t>-1|t>\<cdot\>e<rsup|t>d
        t>>>>
      </eqnarray*>
    </solution*>

    <item>\<#8BBE\>\<#51FD\>\<#6570\> <math|f<around*|(|x|)>>
    \<#5728\>\<#533A\>\<#95F4\> <math|I> \<#4E0A\>\<#6709\>\<#5B9A\>\<#4E49\>\<#FF0C\>\<#5E76\>\<#79F0\>

    <\equation*>
      W<rsub|f><around*|(|\<delta\>|)>=sup<rsub|x<rprime|'>,x<rprime|''>\<in\>I,<around*|\||x<rprime|'>-x<rprime|''>|\|>\<less\>\<delta\>><around*|\||f<around*|(|x<rprime|'>|)>-f<around*|(|x<rprime|''>|)>|\|>
    </equation*>

    \<#4E3A\> <math|f> \<#7684\>\<#8FDE\>\<#7EED\>\<#51FD\>\<#6570\>\<#FF0C\>\<#8BC1\>\<#660E\>:
    <math|f<around*|(|x|)>> \<#5728\> <math|I>
    \<#4E0A\>\<#4E00\>\<#81F4\>\<#8FDE\>\<#7EED\>\<#7684\>\<#5145\>\<#8981\>\<#6761\>\<#4EF6\>\<#4E3A\>
    <math|lim<rsub|\<delta\>\<rightarrow\>0<rsup|+>>W<rsub|f><around*|(|\<delta\>|)>=0>.

    <\proof>
      \<#5148\>\<#8BC1\>\<#5FC5\>\<#8981\>\<#6027\>\<#FF0C\>\<#82E5\>
      <math|f<around*|(|x|)>> \<#5728\> <math|I>
      \<#4E0A\>\<#4E00\>\<#81F4\>\<#8FDE\>\<#7EED\>\<#FF0C\>\<#5219\>
      <math|\<forall\>\<varepsilon\>\<gtr\>0>\<#FF0C\><math|\<exists\>\<delta\><rsub|\<varepsilon\>>\<gtr\>0>\<#FF0C\>\<#4F7F\>\<#5F97\>
      <math|\<forall\>x<rsub|1>,x<rsub|2>\<in\>I> \<#4E14\>
      <math|<around*|\||x<rsub|1>-x<rsub|2>|\|>\<less\>\<delta\><rsub|\<varepsilon\>>>\<#FF0C\>\<#7686\>\<#6EE1\>\<#8DB3\>
      <math|<around*|\||f<around*|(|x<rsub|1>|)>-f<around*|(|x<rsub|2>|)>|\|>\<less\>\<varepsilon\>>.
      \<#6545\>\<#800C\> <math|\<forall\>\<delta\>\<in\><around*|(|0,\<delta\><rsub|\<varepsilon\>>|)>>\<#FF0C\>\<#6709\>
      <math|W<rsub|f><around*|(|\<delta\>|)>\<less\>\<varepsilon\>>.
      \<#6B64\>\<#5373\> <math|lim<rsub|\<delta\>\<rightarrow\>0<rsup|+>>W<rsub|f><around*|(|\<delta\>|)>=0>.

      \<#518D\>\<#8BC1\>\<#5145\>\<#5206\>\<#6027\>\<#FF0C\>\<#7531\>
      <math|lim<rsub|\<delta\>\<rightarrow\>0<rsup|+>>W<rsub|f><around*|(|\<delta\>|)>=0>,
      <math|\<forall\>\<varepsilon\>\<gtr\>0>,
      <math|\<exists\>\<delta\><rsub|\<varepsilon\>>\<gtr\>0>\<#FF0C\>
      \<#4F7F\>\<#5F97\> <math|\<forall\>\<delta\>\<in\><around*|(|0,\<delta\><rsub|\<varepsilon\>>|)>>
      \<#5747\>\<#6EE1\>\<#8DB3\> <math|W<rsub|f><around*|(|\<delta\>|)>\<less\>\<varepsilon\>>,
      \<#4E8E\>\<#662F\> \<#5BF9\>\<#4E8E\>
      <math|\<forall\>x<rsub|1>,x<rsub|2>\<in\>I> \<#4E14\>\<#7B26\>\<#5408\>
      <math|<around*|\||x<rsub|1>-x<rsub|2>|\|>\<less\><frac|1|2>\<delta\><rsub|\<varepsilon\>>\<less\>\<delta\><rsub|\<varepsilon\>>>\<#FF0C\>\<#5747\>\<#6709\>
      <math|<around*|\||x<rsub|1>-x<rsub|2>|\|>\<leqslant\>W<rsub|f><around*|(|<frac|1|2>\<delta\><rsub|\<varepsilon\>>|)>\<less\>W<rsub|f><around*|(|\<delta\>|)>\<less\>\<varepsilon\>>\<#FF0C\>\<#5373\>
      <math|f<around*|(|x|)>> \<#5728\> <math|I>
      \<#4E0A\>\<#4E00\>\<#81F4\>\<#8FDE\>\<#7EED\>.
    </proof>

    <item>\<#8BBE\>\<#4E8C\>\<#5143\>\<#51FD\>\<#6570\>

    <\equation*>
      f<around*|(|x,y|)>=<tabular|<tformat|<table|<row|<cell|<around*|{|<tabular|<tformat|<table|<row|<cell|<around*|(|x<rsup|2>+y<rsup|2>|)>sin<frac|1|x<rsup|2>+y<rsup|2>>,>|<cell|x<rsup|2>+y<rsup|2>\<neq\>0>>|<row|<cell|0,>|<cell|x<rsup|2>+y<rsup|2>=0>>>>>|\<nobracket\>>>>>>>
    </equation*>

    \<#8BA8\>\<#8BBA\> <math|f<around*|(|x,y|)>> \<#5728\>\<#539F\>\<#70B9\>
    <math|<around*|(|0,0|)>> \<#5904\>\<#7684\>\<#8FDE\>\<#7EED\>\<#6027\>\<#FF0C\>\<#53EF\>\<#5FAE\>\<#6027\>\<#4EE5\>\<#53CA\>\<#504F\>\<#5BFC\>\<#6570\>\<#5728\>\<#539F\>\<#70B9\>
    <math|<around*|(|0,0|)>> \<#5904\>\<#7684\>\<#8FDE\>\<#7EED\>\<#6027\>.

    <item>\<#8BBE\> <math|f<around*|(|x|)>\<geqslant\>0>,
    <math|g<around*|(|x|)>\<geqslant\>0>,
    \<#4E24\>\<#4E2A\>\<#51FD\>\<#6570\>\<#5728\> <math|<around*|[|a,b|]>>
    \<#4E0A\>\<#90FD\>\<#8FDE\>\<#7EED\>\<#FF0C\>\<#8BC1\>\<#660E\>:

    <\equation*>
      lim<rsub|n\<rightarrow\>\<infty\>><around*|[|<big|int><rsub|a><rsup|b><around*|(|f<around*|(|x|)>|)><rsup|n>\<cdot\>g<around*|(|x|)>d
      x|]><rsup|<frac|1|n>>=max<rsub|a\<leqslant\>x\<leqslant\>b>f<around*|(|x|)>
    </equation*>

    <\proof>
      \;
    </proof>

    \;

    <item>\<#8BBE\> <math|a\<less\>b>, <math|f<around*|(|x|)>> \<#5728\>
    <math|<around*|(|a,b|)>> \<#4E0A\>\<#8FDE\>\<#7EED\>.

    <\enumerate-roman>
      <item>\<#5047\>\<#8BBE\> <math|x<rsub|0>\<in\><around*|(|a,b|)>>\<#FF0C\><math|f<around*|(|x|)>>
      \<#5728\>\<#70B9\> <math|x<rsub|0>>
      \<#7684\>\<#67D0\>\<#4E2A\>\<#53BB\>\<#5FC3\>\<#53F3\>\<#90BB\>\<#57DF\>\<#5185\>\<#53EF\>\<#5BFC\>\<#FF0C\>\<#4E14\>\<#5B58\>\<#5728\>
      <math|A\<in\>R>\<#FF0C\> \<#4F7F\>\<#5F97\>
      <math|lim<rsub|x\<rightarrow\>x<rsub|0><rsup|+>>f<rprime|'><around*|(|x|)>=A>\<#FF0C\>\<#8BC1\>\<#660E\>:
      <math|f<around*|(|x|)>> \<#5728\>\<#70B9\> <math|x<rsub|0>>
      \<#5904\>\<#7684\>\<#53F3\>\<#5BFC\>\<#6570\>
      <math|f<rsub|+><rsup|<rprime|'>><around*|(|x<rsub|0>|)>>
      \<#5B58\>\<#5728\>\<#4E14\>\<#4E3A\> <math|A>.

      <item>\<#8BBE\> <math|f<around*|(|x|)>> \<#5728\>
      <math|<around*|(|a,b|)>> \<#5185\>\<#5904\>\<#5904\>\<#53EF\>\<#5BFC\>\<#FF0C\>\<#90A3\>\<#4E48\>
      <math|\<forall\>x\<in\><around*|(|a,b|)>>\<#FF0C\>\<#8BC1\>\<#660E\>:
      <math|x> \<#8981\>\<#4E48\>\<#662F\> <math|f<rprime|'><around*|(|x|)>>
      \<#7684\>\<#8FDE\>\<#7EED\>\<#70B9\>\<#FF0C\>\<#8981\>\<#4E48\>\<#662F\>
      <math|f<rprime|'><around*|(|x|)>> \<#7684\>\<#7B2C\>\<#4E8C\>\<#7C7B\>\<#95F4\>\<#65AD\>\<#70B9\>.
    </enumerate-roman>

    <item>\<#5C06\> <math|f<around*|(|x|)>=<around*|\||x|\|>> \<#5728\>
    <math|<around*|[|-\<pi\>,\<pi\>|]>> \<#4E0A\>\<#5C55\>\<#5F00\>\<#6210\>\<#5085\>\<#91CC\>\<#53F6\>\<#7EA7\>\<#6570\>\<#FF0C\>\<#5E76\>\<#7531\>\<#6B64\>\<#6C42\>\<#51FA\>
    <math|<big|sum><rsub|n=1><rsup|\<infty\>><frac|1|n<rsup|2>>>.

    <item>\<#8BBE\> <math|\<Omega\>> \<#662F\>\<#7531\> <math|R<rsup|3>>
    \<#4E2D\>\<#7B80\>\<#5355\>\<#5149\>\<#6ED1\>\<#95ED\>\<#66F2\>\<#7EBF\>
    <math|\<Sigma\>> \<#6240\>\<#56F4\>\<#6210\>\<#7684\>\<#6709\>\<#754C\>\<#533A\>\<#57DF\>.

    <\enumerate-roman>
      <item>

      <item>

      <item>
    </enumerate-roman>
  </enumerate-numeric>

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|activity-name|<tuple|?|1>>
    <associate|auto-1|<tuple|?|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|\<#90D1\>\<#5DDE\>\<#5927\>\<#5B66\>2022\<#5E74\>\<#6570\>\<#5B66\>\<#5206\>\<#6790\>\<#8003\>\<#7814\>\<#8BD5\>\<#9898\>\<#53CA\>\<#89E3\>\<#7B54\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|1fn>
    </associate>
  </collection>
</auxiliary>